// bmp.c
#include "../include/bmp.h"
#include <stdio.h>
#include <stdlib.h>


#define BMP_HEADER_SIZE 54
#define BMP_TYPE 0x4D42
#define BITS_PER_PIXEL 24
#define DEFAULT_COMPRESSION 0
#define PIXEL_INFO_SIZE 40



enum read_status read_from_bmp_file(const char* filename, struct image* img){
    FILE* file = fopen(filename, "rb");
    if (!file){
        return READ_ERROR;
    }
    enum read_status status = from_bmp(file, img);
    fclose(file);
    return status;
}


enum write_status write_to_bmp_file(const char* filename, struct image const* img) {
    FILE* file = fopen(filename, "wb");
    if (!file) {
        return WRITE_ERROR;
    }
    enum write_status status = to_bmp(file, img);
    fclose(file);
    return status;
}





enum read_status from_bmp(FILE* in, struct image* img){
    if (!in || !img) {
        return READ_ERROR;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_TYPE || header.biBitCount != BITS_PER_PIXEL || header.biCompression != DEFAULT_COMPRESSION) {
        return READ_INVALID_SIGNATURE;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) {
        return READ_ERROR;
    }

    // Вычисление паддинга для строки пикселей в BMP
    size_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;

    for (size_t i = 0; i < img->height; ++i) {
        size_t row = img->height - 1 - i;
        if (fread(&img->data[row * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            free(img->data);
            return READ_INVALID_BITS;
        }
        // Пропускаем паддинг в файле
        fseek(in, (long)padding, SEEK_CUR);
    }

    return READ_OK;
}



enum write_status to_bmp(FILE* out, struct image const* img) {
    if (!out || !img) {
        return WRITE_ERROR;
    }

    // Вычисление размера паддинга для каждой строки и общего размера файла
    size_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;
    size_t padded_row_size = img->width * sizeof(struct pixel) + padding;
    size_t pixel_data_size = img->height * padded_row_size;

    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfileSize = BMP_HEADER_SIZE + pixel_data_size,
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE,
            .biSize = PIXEL_INFO_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = DEFAULT_COMPRESSION,
            .biSizeImage = pixel_data_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    // Запись заголовка в файл
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    // Буфер для паддинга
    unsigned char padding_buf[3] = {0, 0, 0};

    // Запись пиксельных данных с паддингом
    for (size_t i = 0; i < img->height; ++i) {
        size_t row = img->height - 1 - i;
        if (fwrite(&img->data[row * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (padding > 0 && fwrite(padding_buf, 1, padding, out) != padding) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
