#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp.h"
#include "../include/transform.h"

int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Wrong format! \nUse: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    // Допустимые значения угла
    int valid_angles[] = {0, 90, 180, 270, -90, -180, -270};
    int angle = atoi(argv[3]);
    int size = sizeof(valid_angles) / sizeof(valid_angles[0]);
    int validAngle = 0;

    // Проверяем, что указанный угол является допустимым
    for (int i = 0; i < size; ++i) {
        if (angle == valid_angles[i]) {
            validAngle = 1;
            break;
        }
    }

    if (!validAngle) {
        fprintf(stderr, "Error: Invalid angle. Valid angles are 0, 90, 180, 270, -90, -180, -270.\n");
        return 1;
    }

    struct image source_img;
    enum read_status read_stat = read_from_bmp_file(argv[1], &source_img);
    if (read_stat != READ_OK) {
        fprintf(stderr, "Error reading image from file\n");
        return 1;
    }

    struct image transformed_img = rotate(source_img, angle);
    image_free(&source_img); // Освобождение исходного изображения

    enum write_status write_stat = write_to_bmp_file(argv[2], &transformed_img);
    if (write_stat != WRITE_OK) {
        fprintf(stderr, "Error writing image to file\n");
        image_free(&transformed_img); // Освобождение памяти трансформированного изображения
        return 1;
    }

    image_free(&transformed_img); // Освобождение памяти трансформированного изображения
    printf("Image transformation successful.\n");

    return 0;
}
