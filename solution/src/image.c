#include "../include/image.h"
#include <stdlib.h>


void image_free(struct image *img) {

    free(img->data); // Освобождение памяти, выделенной для пиксельных данных
    img->data = NULL; // Задаем указатель на NULL после освобождения памяти

}
