#include "../include/transform.h"
#include <stdlib.h>


struct image rotate(struct image const source, int angle) {
    struct image rotated = {0}; // Инициализируем нулями для безопасности

    // Нормализация угла
    angle = ((angle % 360) + 360) % 360;

    // Определение размеров нового изображения
    if (angle == 90 || angle == 270) {
        rotated.width = source.height;
        rotated.height = source.width;
    } else {
        rotated.width = source.width;
        rotated.height = source.height;
    }

    // Выделение памяти
    rotated.data = malloc(rotated.width * rotated.height * sizeof(struct pixel));
    if (!rotated.data) {
        exit(EXIT_FAILURE); // В случае ошибки завершаем работу программы
    }

    // Поворот изображения
    for (uint32_t y = 0; y < source.height; ++y) {
        for (uint32_t x = 0; x < source.width; ++x) {
            struct pixel current_pixel = source.data[y * source.width + x];
            uint32_t new_x, new_y;

            // Вычисляем новые координаты пикселя для разных углов
            switch (angle) {
                case 90:
                    new_x = source.height - y - 1;
                    new_y = x;
                    break;
                case 270:
                    new_x = y;
                    new_y = source.width - x - 1;
                    break;
                case 180:
                    new_x = source.width - x - 1;
                    new_y = source.height - y - 1;
                    break;
                default: // 0 градусов или нормализованный угол, который не изменяет изображение
                    new_x = x;
                    new_y = y;
                    break;
            }

            rotated.data[new_y * rotated.width + new_x] = current_pixel;
        }
    }

    return rotated;
}
